import random

print("WELCOME TO RANDOM NUMBER GUESSING GAME! \nEnter the range of numbers you want to guess - \n")
low = int(input("Enter lower limit of the range: "))
upper = int(input("Enter upper limit of the range: "))

num = random.randint(low, upper)
print(f"num {num}")
print("\n")

guess = int(input("Enter your guess: "))


# def check_range(num, guess):
#     if guess < low or guess > upper:
#         print(f"Your guess is outside the range! Enter a number between {low} and {upper}")
#         return True
#     else:
#         return False
#

def random_guess_result(num, guess):
    if guess == num:
        return True
    else:
        return False


def check_diff(low, upper):
    # calculate difference between low and upper
    diff = upper - low
    return diff


## main program

diff = check_diff(low, upper)
i = 0

if diff <= 2:
    print("you have 1 chance only")
    random_guess_result(num, guess)
else:
    while i < 2 and not random_guess_result(num, guess):
        guess = int(input("Enter your guess: "))
        random_guess_result(num, guess)
        i += 1
    else:
        print("you are here")

if random_guess_result(num, guess):  # check_range(num, guess) and
    print(f"YES! YOUR GUESS WAS RIGHT! The number was {num} and you guessed {guess}! \nCONGRATULATIONS")
else:
    print(f"OOPS! Wrong guess, the number generated was {num} and you guessed {guess}.\nBetter luck next time!")
