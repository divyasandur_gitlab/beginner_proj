import random

print("WELCOME TO RANDOM NUMBER GUESSING GAME! \nEnter the range of numbers you want to guess - \n")
low = int(input("Enter lower limit of the range: "))
upper = int(input("Enter upper limit of the range: "))

num = random.randint(low, upper)

print("\n")

guess = int(input("Enter your guess: "))


def random_guess(num, guess):
    if guess == num:
        return True
    else:
        return False


if random_guess(num, guess):
    print(f"YES! YOUR GUESS WAS RIGHT! The number was {num} and you guessed {guess}! \nCONGRATULATIONS")
else:
    print(f"OOPS! Wrong guess, the number generated was {num} and you guessed {guess}.\nBetter luck next time!")

